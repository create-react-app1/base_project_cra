import styled from 'styled-components'

export default styled.div`
.slider {
  // position: absolute;
  // top: 50%;
  // left: 50%;
  // transform: translate(-50%, -50%);
  position: relative;
  width: 400px;
  height: 300px;
  box-shadow: 3px 3px 10px rgba(0, 0, 0, 0.2);
}

.wrapper {
  overflow: hidden;
  position: relative;
  background: #222;
  z-index: 1;
}

#items {
  width: 10000px;
  position: relative;
  top: 0;
  left: -400px;
}

#items.shifting {
  transition: left .2s ease-out;
}

.slide {
  width: 400px;
  height: 300px;
  cursor: pointer;
  float: left;
  display: flex;
  flex-direction: column;
  justify-content: center;
  transition: all 1s;
  position: relative;
  background: #FFCF47;
}

.control {
  position: absolute;
  top: 50%;
  width: 40px;
  height: 40px;
  background: #fff;
  border-radius: 20px;
  margin-top: -20px;
  box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.3);
  z-index: 2;
}

.prev,
.next {
  background-size: 22px;
  background-position: center;
  background-repeat: no-repeat;
  cursor: pointer;
  height: 36px;
  width: 36px;
}

.prev {
  background-image: url(https://cdn0.iconfinder.com/data/icons/navigation-set-arrows-part-one/32/ChevronLeft-512.png);
  left: -20px;
}

.next {
  background-image: url(https://cdn0.iconfinder.com/data/icons/navigation-set-arrows-part-one/32/ChevronRight-512.png);
  right: -20px;
}

.prev:active,
.next:active {
  transform: scale(0.8);
}
`