import React, {useState, useEffect} from 'react'

import Style from './style'

export default function ExpandedText({content, initExpanded = false, btnLabels, classes}) {

  const [expanded, setExpanded] = useState(initExpanded)
  const [needExpand, setNeedExpand] = useState(true)
  let expandedTextElement = React.createRef()
  let textElement = React.createRef()

  const expandContent = (e) => {
    expandedTextElement.current.classList.toggle('expanded')
    setExpanded(!expanded)
  }  

  const verifyTextElementSize = () => {
    if(!textElement.current) return 
    
    const textIsBig = textElement.current.clientHeight < textElement.current.scrollHeight
    needExpand !== textIsBig && setNeedExpand(textIsBig)
  }

  window.addEventListener('resize', verifyTextElementSize)

  return (
    <Style ref={expandedTextElement} className={["expanded-text", classes].join(' ').trim()}>
      <div 
        ref={textElement} 
        className={["text-resume", classes].join(' ').trim()} 
        onClick={expandContent}
      >
        {content}
      </div>
      
      { needExpand && <div onClick={expandContent} className={["hidden-label", classes].join(' ').trim()} />}
      
      {
        needExpand && (
          <div 
            className={["toggle-text", classes].join(' ').trim()} 
            onClick={expandContent}
          >
            {expanded ? btnLabels.expanded : btnLabels.resume}
          </div>
        )
      }
    </Style>
  )
}