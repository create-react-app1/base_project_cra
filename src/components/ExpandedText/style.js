import styled from 'styled-components'

export default styled.div`

.text-resume {
  display: block; 
  display: -webkit-box; 
  -webkit-line-clamp: 2; 
  -webkit-box-orient: vertical;
  overflow: hidden; 
  text-overflow: ellipsis;
}

.hidden-label {
  height: 10px;
  top: -5px;
  position: relative;
  filter: blur(5px);
  background: white;
}

&.expanded {
  .text-resume {
    overflow: inherit;
    -webkit-line-clamp: inherit;
  }

  .hidden-label {
    height: 0px;
  }
}

`