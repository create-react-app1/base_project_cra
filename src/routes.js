import React from 'react'
import { BrowserRouter, Route, Switch} from 'react-router-dom'

import HomePage from './pages/HomePage'
import ScrollVerifyEnd from './pages/Test/ScrollVerifyEnd'
import ExpandedText from './pages/Test/ExpandedText'
import DragableSlider from './pages/Test/DragableSlider'

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={HomePage} />
        <Route path="/test/scroll" component={ScrollVerifyEnd} />
        <Route path="/test/expanded" component={ExpandedText} />
        <Route path="/test/slider" component={DragableSlider} />
      </Switch>
    </BrowserRouter>
  )
}