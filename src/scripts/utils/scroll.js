// Horizontal - X

const addScrollVerifyEndX = (element, callback, options = {params: {}, removeListener: false} ) => {
  setElement(element, callback, options, 'horizontal')
  element.removeEventListener("scroll", horizontalScroll)
  element.addEventListener("scroll", horizontalScroll)
}

const horizontalScroll = (e) => {
  const target = e.target
  const { callback, options } = target.scrollConfigs.horizontal
  const currentLeft = target.scrollLeft
  const scrollWidth = target.setScrollLimits.x
  
  if(currentLeft >= scrollWidth) {
    callback && callback(options.params)
    options.removeListener && target.removeEventListener("scroll", horizontalScroll);
  }
}


// Vertical - Y

const addScrollVerifyEndY = (element, callback, options = {params: {}, removeListener: false}) => {
  setElement(element, callback, options, 'vertical')
  element.removeEventListener('scroll', verticalScroll)
  element.addEventListener('scroll', verticalScroll)
}

const verticalScroll = (e) => {
  const target = e.target
  const { callback, options } = target.scrollConfigs.vertical
  const currentTop = target.scrollTop
  const scrollHeight = target.setScrollLimits.y

  if(currentTop >= scrollHeight) {
    callback && callback(options.params)
    options.removeListener && target.removeEventListener("scroll", verticalScroll);
  }
}


// Aux Functions

const setElement = (element, callback, options, scrollOrientation) => {
  setScrollLimit(element)
  setElementConfig(element, callback, options, scrollOrientation)
}

const setScrollLimit = (element) => {

  // element.scrollLimits = { 
  //   ...element.scrollLimits, 
  //   x: Math.max(element.scrollWidth, element.offsetWidth, element.clientWidth) -
  //   Math.min(element.scrollWidth, element.offsetWidth, element.clientWidth)
  // }

  // current -> scrollLeft
  // total -> scrollWidth
  // viewd width -> clientHeight

  const {scrollWidth, clientWidth, scrollHeight, clientHeight} = element

  element.setScrollLimits = {
    x: scrollWidth - clientWidth,
    y: scrollHeight - clientHeight,
  }

}

const setElementConfig = (element, callback, options, scrollOrientation) => {
  element.scrollConfigs = {
    ...element.scrollConfigs,
    [scrollOrientation]: {
      callback,
      options,
    }
  }
}

export default {
  addScrollVerifyEndX,
  addScrollVerifyEndY,
}