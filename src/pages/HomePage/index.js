import React from 'react';
import PropTypes from 'prop-types'

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { clickButton, changeTheme } from '@actions';

// API
import exampleAPI from '@services/example-api'

// Style
import Style from './style'

// Components

function HomePage(props) {
  let mainInput = React.createRef()

  const updateText = () => props.clickButton(mainInput.current.value)
  const changeTheme = () => props.changeTheme(props.theme === 'main' ? 'dark' : 'main')

  const { newValue } = props

  return (
    <Style>
      <div className="App">
        <input ref={mainInput} type='text' />
        <button onClick={() => updateText()}>
          Click me!
        </button>
        <h1>{newValue}</h1>

        <br />

        <button onClick={() => changeTheme()}>Change the Theme!</button>
      </div>
    </Style>
  )

}

const mapStateToProps = store => ({
  newValue: store.clickState.newValue,
  theme: store.themeState.theme,
})

const mapDispatchToProps = dispatch => 
  bindActionCreators({
    clickButton,
    changeTheme,
  }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(HomePage)
