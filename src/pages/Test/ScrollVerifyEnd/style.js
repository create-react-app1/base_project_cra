import styled from 'styled-components'

export default styled.div`

.scroll-end-verify {
  height: 100px;
  width: 300px;
  overflow: auto;
}

.paragraph {
  width: 400px;
}

`