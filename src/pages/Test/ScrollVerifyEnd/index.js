import React, { useEffect } from 'react'

import Style from './style'

import Scroll from '@utils/scroll'

export default function Test(props) {
  let scrollEndVerifyElement = React.createRef()

  // Similar ao componentDidMount e componentDidUpdate:
  useEffect(() => {
    // Atualiza o titulo do documento usando a API do browser
    Scroll.addScrollVerifyEndY(scrollEndVerifyElement.current, ()=> {console.log("End of Scroll Vertical")})
    Scroll.addScrollVerifyEndX(scrollEndVerifyElement.current, ()=> {console.log("End of Scroll Horizontal")})
  });

  return (
    <Style>
      <div ref={scrollEndVerifyElement} className="scroll-end-verify">
        <div className="paragraph">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eget justo nibh. Fusce ultrices laoreet malesuada. Ut nunc arcu, aliquam ut tincidunt quis, varius a libero. Curabitur placerat elit eget dapibus consectetur. In posuere velit eu elit ullamcorper tristique. Suspendisse tristique congue tincidunt. Cras eleifend dui magna, at condimentum mauris lacinia porttitor. Quisque at iaculis magna. Vivamus orci sem, maximus vitae nulla lobortis, maximus varius erat. Donec nec placerat nunc, at laoreet enim.

          Morbi a nulla felis. Aenean ullamcorper magna in nunc vestibulum eleifend. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aliquam eu dui eu est venenatis rhoncus. Donec cursus tincidunt ante ac tincidunt. Quisque malesuada velit eget lorem vehicula viverra. Fusce interdum tempor lectus, eget pulvinar elit laoreet vel. Cras finibus dignissim nibh, a sodales ipsum tempor id. Suspendisse potenti. Pellentesque vitae augue et nisl varius mattis nec in risus. Donec mattis justo nec metus blandit convallis. Curabitur ultrices aliquam eleifend. Nam ut ante sem.

          Sed vel orci tellus. Quisque rutrum, purus quis dictum suscipit, ex nisi vestibulum purus, sed fermentum mi augue cursus erat. Etiam id convallis arcu. Donec vitae mi eu velit ultricies mattis. Praesent vulputate volutpat velit, quis pretium nulla imperdiet sit amet. Praesent vel dignissim orci. Suspendisse non massa leo.

          Sed tempor, massa ut sollicitudin maximus, leo lorem pulvinar massa, sit amet ornare mi urna id sapien. Suspendisse convallis at nisi sed sodales. Cras dignissim viverra auctor. Ut ac nisi blandit, placerat lorem in, pretium nisi. Ut fringilla euismod elit, sit amet placerat risus. Proin vitae purus ultricies, imperdiet velit ut, feugiat nunc. Ut iaculis molestie sagittis. Morbi commodo lorem eget diam vehicula auctor. Maecenas non egestas massa. Morbi tempus dictum tincidunt. Aliquam quis congue augue, vitae vulputate quam. Etiam scelerisque hendrerit orci, id porttitor mi dignissim in. Duis venenatis bibendum leo, nec posuere enim hendrerit non. Morbi sodales pharetra risus, quis hendrerit erat dignissim ut.
        </div>
      </div>
    </Style>
  )
}