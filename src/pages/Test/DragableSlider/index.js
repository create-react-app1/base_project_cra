import React from 'react'

import Style from './style'

import DragableSlider from '@components/DragableSlider'

import img1 from './imgs/download.png'
import img2 from './imgs/download.jpeg'

export default function Test(props) {

  return (
    <Style>

      <DragableSlider items={["Slide 1", "Slide 2", "Slide 3", "Slide 4", "Slide 5", ]}/>

      <DragableSlider items={["Slide 1", "Slide 2", "Slide 3", "Slide 4", "Slide 5", ]} classes="test"/>

      <DragableSlider items={[img1, img2].map(img => <img src={img} />)} classes="test1"/>

    </Style>
  )
}