import { combineReducers } from 'redux';

import { clickReducer } from './clickReducer';
import { themeReducer } from './themeReducer';

export const Reducers = combineReducers({
  clickState: clickReducer,
  themeState: themeReducer,
});