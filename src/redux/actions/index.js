import { CLICK_UPDATE_VALUE, CHANGE_THEME } from './actionTypes';

export const clickButton = value => ({
    type: CLICK_UPDATE_VALUE,
    newValue: value
});

export const changeTheme = value => ({
    type: CHANGE_THEME,
    theme: value
})