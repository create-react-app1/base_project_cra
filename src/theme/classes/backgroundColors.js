import { createGlobalStyle } from "styled-components";

import colors from "@theme/colors";

export default createGlobalStyle`
  .bg-tropicalRainForest { background: ${colors.tropicalRainForest}; }
  .bg-grey { background: ${colors.grey}; }
  .bg-vividTangerine { background: ${colors.vividTangerine}; }
  .bg-caramel { background: ${colors.caramel}; }
  .bg-coldPurple { background: ${colors.coldPurple}; }
`