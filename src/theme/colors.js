/**
  Colors

  Obs: Mostly of the color variables names are from aproximates names.
  Reference: https://www.htmlcsscolor.com
**/

let colors = {
  black: '#000000',
  caramel: '#FCDA92',
  coldPurple: '#9C8CB9',
  gainsboro: '#E0E0E0',
  grey: '#868686',
  tropicalRainForest: '#007f56',
  vividTangerine: '#FE9481',
  whisper: '#EEEEEE',
  white: '#FFFFFF',
  
}

export default {
  ...colors,

  main: {
    primary: colors.tropicalRainForest,
    secundary: colors.grey,

    background: colors.white,
  },

  dark: {
    primary: colors.tropicalRainForest,
    secundary: colors.whisper,

    background: colors.black,
  }
};
