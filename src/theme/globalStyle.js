import { createGlobalStyle } from "styled-components";
import myTheme from '@theme'

export default createGlobalStyle`
  body {    
    margin: 0;

    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    background-color: ${props => myTheme.colors[props.theme.name].background} !important;
    color: ${props => myTheme.colors[props.theme.name].primary};
  }
  
  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, "Courier New",
      monospace;
  }

  @media (min-width: 1200px) {
    .container {
        max-width: 1200px !important;
    }
  }

  img, picture, video, embed {
    max-width: 100%;
  }
`
